//
//  CollectionController.m
//  ViewFramework
//
//  Created by J Henry on 8/6/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "CollectionController.h"
#import "CollectionsService.h"
#import "UserCollectionInfo.h"
#import "CollectionIssueInfo.h"
#import "VolumeInfo.h"

@interface CollectionController ()

@end

@implementation CollectionController

NSNumber *m_CollectionId;
NSNumber *m_UserId;
static int m_Page;
NSArray *volumes;

UserCollectionInfo *m_Collection;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	[[self spinner_GetCollectionVolumes] startAnimating];
	
	m_UserId = [CollectionsService GetCurrentUserId];
	m_CollectionId = [CollectionsService GetCurrentCollectionId];
	
	m_Page = [CollectionsService GetCurrentPage];
	
	[self GetCollection];

	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Volumes" style:UIBarButtonItemStylePlain target:nil action:nil];
	
	    [super viewDidLoad];
}
-(void)GetCollection
{
	[CollectionsService GetCollection:m_UserId
						 collectionId:m_CollectionId
								 page:[NSNumber numberWithInt:m_Page]
							  success:^(UserCollectionInfo *collection)
							  {
									[[self spinner_GetCollectionVolumes] stopAnimating];
							  
								  m_Collection = collection;
								  [CollectionsService SetCurrentCollection:m_Collection];
								  
								  self.title = [m_Collection collectionName];
								  
								  volumes = m_Collection.volumes;
								  
								  [self.tableView reloadData];
								 								  
							  } failure:^(NSError *error) {
								  
								  [[self spinner_GetCollectionVolumes] stopAnimating];
								  
								  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																				  message:[error description]
																				 delegate:nil
																		cancelButtonTitle:@"OK"
																		otherButtonTitles:nil];
								  
								  [alert show];
							  }];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [m_Collection.volumes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *collectionCell = @"CollectionCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
		{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
		}
	
	if([m_Collection.volumes count] > indexPath.row)
		{
			VolumeInfo *selectedVolume =  [m_Collection.volumes objectAtIndex:indexPath.row];
			cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",
								   selectedVolume.volumeName,
								   selectedVolume.volumeStartYear];
		}
	
	cell.textColor = [UIColor whiteColor];
	
    return cell;
}

#pragma mark - Table view delegate
/*
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSString *segueId = segue.identifier;
	
	if([segueId isEqualToString:@"segue_ShowCollection"])
		{
			NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		m_Collection = [CollectionsService GetCurrentCollection];
			VolumeInfo *selectedVolume =  [m_Collection.volumes objectAtIndex:indexPath.row];
			[CollectionsService SetCurrentVolumeId:selectedVolume.volumeID];

		}
}
*/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	m_Collection = [CollectionsService GetCurrentCollection];
	VolumeInfo *selectedVolume =  [m_Collection.volumes objectAtIndex:indexPath.row];
	[CollectionsService SetCurrentVolumeId:selectedVolume.volumeID];
	[CollectionsService SetCurrentVolumeName:selectedVolume.volumeName];
	
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Volumes" style:UIBarButtonItemStylePlain target:nil action:nil];
	
	[self performSegueWithIdentifier:@"segue_ShowTableView" sender:self];
}


@end
