//
//  CollectionsController.h
//  ViewFramework
//
//  Created by J Henry on 7/29/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//


@interface CollectionsController : UITableViewController	

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner_GetUserCollections;
@end
