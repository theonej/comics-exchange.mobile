//
//  CreateCollectionController.h
//  ViewFramework
//
//  Created by J Henry on 1/6/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateCollectionController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *text_CollectionName;
@property (strong, nonatomic) IBOutlet UITextView *text_Description;
@property (strong, nonatomic) IBOutlet UISwitch *switch_IsPubliclyViewable;
@property (strong, nonatomic) IBOutlet UIButton *button_CreateCollection;

- (IBAction)text_CollectionName:(UITextField *)sender;
-(IBAction)button_CreateCollection_Click:(id)sender;
@end
