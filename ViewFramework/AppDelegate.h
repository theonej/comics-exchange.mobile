//
//  AppDelegate.h
//  ViewFramework
//
//  Created by J Henry on 7/29/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
