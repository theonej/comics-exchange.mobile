//
//  UtilityServices.m
//  Comics-Exchange.Mobile
//
//  Created by J Henry on 4/28/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "UtilityServices.h"
#import "RestKit/RestKit.h"
#import "ErrorInfo.h"
#import "StorageServices.h"
#import "CollectorInfo.h"

@implementation UtilityServices

+(NSString*)ConvertToBase64String:(NSString*)inputString
{
    NSData *theData = [inputString dataUsingEncoding: NSASCIIStringEncoding];
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
	
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
	
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
	
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
			
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
		
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
	
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
}

+(NSURL*)GetBaseUrl
{
	NSString *baseUri = @"https://api.comics-exchange.com";
	NSURL *baseUrl = [NSURL URLWithString:baseUri];
	return baseUrl;
}


+(RKObjectMapping*)BuildErrorObjectMapping
{
	RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[ErrorInfo class]];
	[errorMapping addAttributeMappingsFromDictionary:@{
													   @"message":@"message"
													   }];
	
	return errorMapping;
}

+(RKResponseDescriptor*)BuildErrorResponseDescriptor
{
	RKObjectMapping *errorMapping = [self BuildErrorObjectMapping];
	
	RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:errorMapping																			  pathPattern:nil
																  keyPath:nil
																  statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError)];
	return errorDescriptor;
}

+(NSString*)GetUserAuthenticationToken
{
	CollectorInfo *collector = [StorageServices  GetCollectorInfo];
	
	return collector.authenticationTicket;
}
@end
