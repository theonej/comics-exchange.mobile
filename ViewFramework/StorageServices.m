//
//  StorageServices.m
//  ViewFramework
//
//  Created by J Henry on 1/5/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "StorageServices.h"

@implementation StorageServices

NSString *COLLECTOR_DATA_OBJECT_KEY = @"CollectorData";

+(CollectorInfo*)GetCollectorInfo
{
	NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
	
	NSData *collectorData = [userData objectForKey:COLLECTOR_DATA_OBJECT_KEY];
	
	CollectorInfo *collector = (CollectorInfo*)[NSKeyedUnarchiver unarchiveObjectWithData:collectorData];
	
	return collector;
}

+(void)SaveCollectorInfo:(CollectorInfo*)collector
{
	NSData *collectorData = [NSKeyedArchiver archivedDataWithRootObject:collector];
	
	NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
	
	[userData setObject:collectorData forKey:COLLECTOR_DATA_OBJECT_KEY];
}

+(void)ClearCollectorInfo
{
	NSUserDefaults *userData = [NSUserDefaults standardUserDefaults];
	
	[userData setObject:nil forKey:COLLECTOR_DATA_OBJECT_KEY];
}

@end
