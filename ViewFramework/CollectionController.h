//
//  CollectionController.h
//  ViewFramework
//
//  Created by J Henry on 8/6/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

@interface CollectionController : UITableViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner_GetCollectionVolumes;
@end
