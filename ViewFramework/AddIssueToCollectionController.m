//
//  AddIssueToCollectionController.m
//  ViewFramework
//
//  Created by J Henry on 8/13/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "AddIssueToCollectionController.h"
#import "SearchService.h"
#import "CollectionsService.h"
#import "UserCollectionInfo.h"
#import "IssueInfo.h"

@interface AddIssueToCollectionController ()
	@property (strong, nonatomic) IBOutlet UIBarButtonItem *button_AddIssueToCollections;

- (IBAction)button_AddIssueToCollections_Click:(id)sender;

@end

@implementation AddIssueToCollectionController

NSArray *m_UserCollections;
IssueInfo *m_CurrentIssue;
NSNumber *m_CurrentUserId;
NSMutableArray *m_SelectedCollections;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	m_SelectedCollections = [[NSMutableArray alloc] init];
	[self button_AddIssueToCollections].enabled = NO;
	
	m_CurrentIssue = [SearchService GetCurrentIssue];
	m_CurrentUserId = [CollectionsService GetCurrentUserId];
	
	[CollectionsService GetUserCollections:m_CurrentUserId
								   success:^(NSArray *collections)
									 {
										m_UserCollections = collections;
										[self.tableView reloadData];
									 }
								   failure:^(NSError *error)
									 {
										 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																						 message:[error description]
																						delegate:nil
																			   cancelButtonTitle:@"OK"
																			   otherButtonTitles:nil];
										 
										 [alert show];
									 }
	 ];

    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_UserCollections count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *collectionCell = @"CollectionCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
		{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
		}
	
	UserCollectionInfo *selectedCollection =  [m_UserCollections objectAtIndex:indexPath.row];
	cell.textLabel.text = selectedCollection.collectionName;
	cell.textColor = [UIColor whiteColor];
	
	return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *indexPathForCell = [tableView cellForRowAtIndexPath:indexPath];
	
	UserCollectionInfo *selectedCollection =  [m_UserCollections objectAtIndex:indexPath.row];
	NSNumber *collectionId = selectedCollection.collectionId;
    if (indexPathForCell.accessoryType == UITableViewCellAccessoryNone)
	{
        indexPathForCell.accessoryType = UITableViewCellAccessoryCheckmark;
		
		[m_SelectedCollections addObject:collectionId];
	
		[self button_AddIssueToCollections].enabled = YES;
    }
	else
	{
		[m_SelectedCollections removeObject:collectionId];
	
		if([m_SelectedCollections count] == 0)
		{
			[self button_AddIssueToCollections].enabled = NO;
		}
        indexPathForCell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (IBAction)button_AddIssueToCollections_Click:(id)sender
{
	for(id collectionId in m_SelectedCollections)
		{
			[CollectionsService AddIssueToCollection:m_CurrentUserId collectionId:collectionId issueId:m_CurrentIssue.issueID success:^(NSString *message) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
																message:message
															   delegate:nil
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:nil];
				
				[alert show];
			} failure:^(NSError *error) {
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																message:[error description]
															   delegate:nil
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:nil];
				
				[alert show];
			}];
		}
}
@end
