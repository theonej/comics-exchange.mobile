//
//  CollectorInfo.m
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "CollectorInfo.h"

@implementation CollectorInfo

@synthesize authenticationTicket;
@synthesize userId;
@synthesize userName;
@synthesize emailAddress;
@synthesize createdDateTime;
@synthesize userIconURL;
@synthesize password;

-(void)encodeWithCoder:(NSCoder*)coder
{
	[coder encodeObject:authenticationTicket forKey:@"authenticationTicket"];
	[coder encodeObject:userId forKey:@"userId"];
	[coder encodeObject:userName forKey:@"userName"];
	[coder encodeObject:emailAddress forKey:@"emailAddress"];
	[coder encodeObject:createdDateTime forKey:@"createdDateTime"];
	[coder encodeObject:userIconURL forKey:@"userIconURL"];
	[coder encodeObject:password forKey:@"password"];
}

-(id)initWithCoder:(NSCoder*)coder
{
	self.authenticationTicket = [coder decodeObjectForKey:@"authenticationTicket"];
	self.userId = [coder decodeObjectForKey:@"userId"];
	self.userName = [coder decodeObjectForKey:@"userName"];
	self.emailAddress = [coder decodeObjectForKey:@"emailAddress"];
	self.createdDateTime = [coder decodeObjectForKey:@"createdDateTime"];
	self.userIconURL = [coder decodeObjectForKey:@"userIconURL"];
	self.password = [coder decodeObjectForKey:@"password"];
	
	return self;
}
@end
