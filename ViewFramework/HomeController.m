//
//  ViewController.m
//  ViewFramework
//
//  Created by J Henry on 7/29/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "HomeController.h"
#import "SearchService.h"
#import "CollectionsService.h"
#import "StorageServices.h"

@interface HomeController () <UITextFieldDelegate>

@end

@implementation HomeController

NSString* m_VolumeName;
- (IBAction)input_VolumeName:(UITextField *)sender
{
	m_VolumeName = [sender.text stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
}

- (void)viewDidLoad
{
	CollectorInfo *collector = [StorageServices GetCollectorInfo];

	[CollectionsService SetCurrentUserId:collector.userId];
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	NSString *segueId = segue.identifier;
	
	if([segueId isEqualToString:@"segue_ShowVolumeSearchResults"])
		{
			[SearchService SetVolumeSearchTerm:m_VolumeName];
		}
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {

	[theTextField resignFirstResponder];
	
    return YES;
}
@end
