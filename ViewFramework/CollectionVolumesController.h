//
//  CollectionVolumesController.h
//  ViewFramework
//
//  Created by J Henry on 1/6/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionVolumesController : UITableViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner_GetVolumeIssues;
@end
