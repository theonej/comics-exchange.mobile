//
//  VolumeInfo.h
//  ViewFramework
//
//  Created by J Henry on 8/6/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VolumeInfo : NSObject

@property (nonatomic, strong) NSNumber *volumeID;
@property (nonatomic, strong) NSNumber *titleID;
@property (nonatomic, strong) NSString *volumeName;
@property (nonatomic, strong) NSString *volumeNumber;
@property (nonatomic, strong) NSNumber *numberOfIssues;
@property (nonatomic, strong) NSString *volumeStartYear;
@property (nonatomic, strong) NSString *volumeEndYear;
@property (nonatomic, strong) NSString *addedDateTime;

@end
