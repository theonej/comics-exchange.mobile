//
//  VolumeInfo.m
//  ViewFramework
//
//  Created by J Henry on 8/6/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "VolumeInfo.h"

@implementation VolumeInfo

	@synthesize volumeID;
	@synthesize titleID;
	@synthesize volumeName;
	@synthesize volumeNumber;
	@synthesize numberOfIssues;
	@synthesize volumeStartYear;
	@synthesize volumeEndYear;
	@synthesize addedDateTime;

@end
