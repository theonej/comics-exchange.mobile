//
//  SearchService.m
//  ViewFramework
//
//  Created by J Henry on 8/11/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "RestKit/RestKit.h"

#import "SearchService.h"
#import "VolumeInfo.h"
#import "IssueInfo.h"
#import "UtilityServices.h"

@implementation SearchService

NSArray *m_Volumes;
NSArray *m_VolumeIssues;
NSString *m_VolumeSearchTerm;
VolumeInfo *m_CurrentVolume;
int m_CurrentPage;
IssueInfo *m_CurrentIssue;

+(IssueInfo*)GetCurrentIssue
{
	return m_CurrentIssue;
}

+(void)SetCurrentIssue:(IssueInfo*)value
{
	m_CurrentIssue = value;
}

+(int)GetCurrentPage
{
	return m_CurrentPage;
}

+(void)SetCurrentPage:(int)value
{
	m_CurrentPage = value;
}

+(NSString*)GetVolumeSearchTerm
{
	return m_VolumeSearchTerm;
}

+(void)SetVolumeSearchTerm:(NSString*)value
{
	m_VolumeSearchTerm = value;
}

+(VolumeInfo*)GetCurrentVolume
{
	return m_CurrentVolume;
}

+(void)SetCurrentVolume:(VolumeInfo*)value
{
	m_CurrentVolume = value;
}

+(RKObjectManager*)CreateVolumeObjectManager
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];		
	
	RKObjectMapping *volumeMapping = [RKObjectMapping mappingForClass:[VolumeInfo class]];
	[volumeMapping addAttributeMappingsFromDictionary:@{
	 @"volumeID":@"volumeID",
	 @"titleID":@"titleID",
	 @"volumeName":@"volumeName",
	 @"volumeNumber":@"volumeNumber",
	 @"numberOfIssues":@"numberOfIssues",
	 @"volumeStartYear":@"volumeStartYear",
	 @"volumeEndYear":@"volumeEndYear",
	 @"addedDateTime":@"addedDateTime"
	 }];
	
	RKResponseDescriptor *volumeDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:volumeMapping pathPattern:nil keyPath:nil statusCodes:[NSIndexSet indexSetWithIndex:200]];
	
	[objectManager addResponseDescriptor:volumeDescriptor];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];

	return objectManager;
}


+(RKObjectManager*)CreateIssueObjectManager
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKObjectMapping *issueMapping = [RKObjectMapping mappingForClass:[IssueInfo class]];
	[issueMapping addAttributeMappingsFromDictionary:@{
	 @"mobileImageLocation":@"mobileImageLocation",
	 @"issueID":@"issueID",
	 @"issueNumber":@"issueNumber",
	 @"volumeID":@"volumeID",
	 @"issueTitle":@"issueTitle",
	 @"releaseYear":@"releaseYear",
	 @"isDeleted":@"isDeleted",
	 @"addedDateTime":@"addedDateTime",
	 @"updatedDateTime":@"updatedDateTime",
	 @"issueDetailsLink":@"issueDetailsLink"
	 }];
	
	RKResponseDescriptor *issueDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:issueMapping pathPattern:nil keyPath:nil statusCodes:[NSIndexSet indexSetWithIndex:200]];
	
	[objectManager addResponseDescriptor:issueDescriptor];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];

	return objectManager;
}

+(void)SearchVolumes:(NSString *)searchTerm
				page:(NSNumber*)page
			 success:(void(^)(NSArray*))success
			 failure:(void(^)(NSError*))failure
{
	NSString *path = [NSString stringWithFormat:@"/Volumes/?searchTerm=%@", searchTerm];
	
	RKObjectManager *objectManager = [SearchService CreateVolumeObjectManager];
	
	[objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
		 {
			 m_Volumes = [mappingResult array];
			 success(m_Volumes);
		 } failure:^(RKObjectRequestOperation *operation, NSError *error)
		 {
		 failure(error);
		 }
	 ];
}

+(void)GetVolumeIssues:(NSNumber*)volumeId
				  page:(NSNumber*)page
			   success:(void(^)(NSArray*))success
			   failure:(void(^)(NSError*))failure
{
	NSString *path = [NSString stringWithFormat:@"/Volumes/%@/Issues/", volumeId];
	
	RKObjectManager *objectManager = [SearchService CreateIssueObjectManager];
	
	NSLog(@"POST Path: %@", path);
	[objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
		 {
			 m_VolumeIssues = [mappingResult array];
			 success(m_VolumeIssues);
		 } failure:^(RKObjectRequestOperation *operation, NSError *error)
		 {
		 failure(error);
		 }
	 ];
 

}
@end
