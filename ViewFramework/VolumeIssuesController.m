//
//  VolumeIssuesController.m
//  ViewFramework
//
//  Created by J Henry on 8/12/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "SearchService.h"
#import "VolumeIssuesController.h"
#import "VolumeInfo.h"
#import "IssueInfo.h"

@interface VolumeIssuesController ()

@end

@implementation VolumeIssuesController

VolumeInfo *m_CurrentVolume;
NSMutableArray *m_VolumeIssues;
static int m_Page;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	
	[[self spinner_GetVolumeIssues] startAnimating];
	
	m_VolumeIssues = [NSMutableArray array];
	[self.tableView reloadData];

    m_CurrentVolume = [SearchService GetCurrentVolume];
	self.title = m_CurrentVolume.volumeName;
	
	m_Page = [SearchService GetCurrentPage];
	[self GetVolumeIssues];
	
    [super viewDidLoad];

}

-(void)GetVolumeIssues
{
		[SearchService GetVolumeIssues:m_CurrentVolume.volumeID page:[NSNumber numberWithInt:m_Page]
							   success:^(NSArray *volumeIssues)
								{
								
									[[self spinner_GetVolumeIssues] stopAnimating];
								
								   m_VolumeIssues = volumeIssues;
								   [self.tableView reloadData];
								}
							   failure:^(NSError *error)
								{
								
								[[self spinner_GetVolumeIssues] stopAnimating];
								
									UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																					message:[error description]
																				   delegate:nil
																		  cancelButtonTitle:@"OK"
																		  otherButtonTitles:nil];
									
									[alert show];
								}
		 ];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_VolumeIssues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *collectionCell = @"IssueCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
		{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
		}
	
    IssueInfo *selectedIssue =  [m_VolumeIssues objectAtIndex:indexPath.row];
	NSString *title =[NSString stringWithFormat:@"# %@", selectedIssue.issueNumber];
	if(! [selectedIssue.issueTitle  isEqual: @""])
		{
		title = [NSString stringWithFormat:@"# %@ (%@)",
							   selectedIssue.issueNumber,
							   selectedIssue.issueTitle];
		}
	cell.textLabel.text = title;
	
	cell.textColor = [UIColor whiteColor];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	IssueInfo *selectedIssue =  [m_VolumeIssues objectAtIndex:indexPath.row];
	
	[SearchService SetCurrentIssue:selectedIssue];
	[SearchService SetCurrentPage:0];
	
	[self performSegueWithIdentifier:@"segue_ShowImageDetails" sender:self];
}

@end
