//
//  IssueInfo.m
//  ViewFramework
//
//  Created by J Henry on 8/12/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "IssueInfo.h"

@implementation IssueInfo

@synthesize mobileImageLocation;
@synthesize issueID;
@synthesize issueNumber;
@synthesize volumeID;
@synthesize issueTitle;
@synthesize numberOfPages;
@synthesize releaseMonth;
@synthesize releaseYear;
@synthesize isDeleted;
@synthesize addedDateTime;
@synthesize updatedDateTime;
@synthesize issueDetailsLink;

@end
