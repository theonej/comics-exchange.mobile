//
//  CollectionsService.h
//  ViewFramework
//
//  Created by J Henry on 7/31/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserCollectionInfo.h"
#import "CollectionIssueInfo.h"

@interface CollectionsService : NSObject

+(void)GetUserCollections:(NSNumber*)userId
				  success:(void(^)(NSArray*))success
				  failure:(void(^)(NSError*))failure;

+(void)GetCollection:(NSNumber*)userId
					 collectionId:(NSNumber*)collectionId
					 page:(NSNumber*)page
					 success:(void(^)(UserCollectionInfo*))success
					 failure:(void(^)(NSError*))failure;

+(void)GetCollectionVolume:(NSNumber*)userId
			  collectionId:(NSNumber*)collectionId
				  volumeId:(NSNumber*)volumeId
					  page:(NSNumber*)page
				   success:(void(^)(UserCollectionInfo*))success
				   failure:(void(^)(NSError*))failure;

+(void)AddIssueToCollection:(NSNumber*)userId
			  collectionId:(NSNumber*)collectionId
				  issueId:(NSNumber*)issueId
				   success:(void(^)(NSString*))success
				   failure:(void(^)(NSError*))failure;

+(void)CreateNewCollection:(UserCollectionInfo*)collection
				   success:(void(^)(UserCollectionInfo*))success
				   failure:(void(^)(NSError*))failure;

+(NSNumber*)GetCurrentVolumeId;
+(void)SetCurrentVolumeId:(NSNumber*)value;

+(int)GetCurrentPage;
+(void)SetCurrentPage:(int)value;

+(NSNumber*)GetCurrentCollectionId;
+(void)SetCurrentCollectionId:(NSNumber*)value;

+(NSNumber*)GetCurrentUserId;
+(void)SetCurrentUserId:(NSNumber*)value;

+(CollectionIssueInfo*)GetCurrentCollectionIssue;
+(void)SetCurrentCollectionIssue:(CollectionIssueInfo*)value;

+(UserCollectionInfo*)GetCurrentCollection;
+(void)SetCurrentCollection:(UserCollectionInfo*)value;

+(void)SetCurrentVolumeName:(NSString*)volumeName;
+(NSString*)GetCurrentVolumeName;

@end
