//
//  StorageServices.h
//  ViewFramework
//
//  Created by J Henry on 1/5/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectorInfo.h"

@interface StorageServices : NSObject

+(CollectorInfo*)GetCollectorInfo;
+(void)SaveCollectorInfo:(CollectorInfo*)collector;
+(void)ClearCollectorInfo;
@end
