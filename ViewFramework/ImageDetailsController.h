//
//  ImageDetailsController.h
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionIssueInfo.h"

@interface ImageDetailsController : UIViewController
@property (nonatomic, strong) IBOutlet UIImageView *coverImageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner_ImageDownload;
-(IBAction)handleSwipe:(UISwipeGestureRecognizer *)recognizer;
@end
