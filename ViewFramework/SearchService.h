//
//  SearchService.h
//  ViewFramework
//
//  Created by J Henry on 8/11/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VolumeInfo.h"
#import "IssueInfo.h"

@interface SearchService : NSObject

+(NSString*)GetVolumeSearchTerm;
+(void)SetVolumeSearchTerm:(NSString*)value;

+(VolumeInfo*)GetCurrentVolume;
+(void)SetCurrentVolume:(VolumeInfo*)value;

+(int)GetCurrentPage;
+(void)SetCurrentPage:(int)value;

+(IssueInfo*)GetCurrentIssue;
+(void)SetCurrentIssue:(IssueInfo*)value;

+(void)SearchVolumes:(NSString *)searchTerm
					  page:(NSNumber*)page
				   success:(void(^)(NSArray*))success
				   failure:(void(^)(NSError*))failure;

+(void)GetVolumeIssues:(NSNumber*)volumeId
					  page:(NSNumber*)page
				   success:(void(^)(NSArray*))success
				   failure:(void(^)(NSError*))failure;
@end
