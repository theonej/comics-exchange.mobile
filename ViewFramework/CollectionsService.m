//
//  CollectionsService.m
//  ViewFramework
//
//  Created by J Henry on 7/31/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "RestKit/RestKit.h"

#import "CollectionsService.h"
#import "UserCollectionInfo.h"
#import "CollectionIssueInfo.h"
#import "VolumeInfo.h"
#import "UtilityServices.h"

@implementation CollectionsService

NSArray *m_Collections;
static NSNumber* m_CurrentCollectionId;
static NSNumber *m_UserId;
static CollectionIssueInfo *m_CurrentCollectionIssue;
static int m_CurrentPage;
static NSNumber *m_CurrentVolumeId;
static NSString *m_CurrentVolumeName;
static UserCollectionInfo *m_CurrentCollection;

+(void)SetCurrentVolumeName:(NSString*)volumeName
{
	m_CurrentVolumeName =volumeName;
}
+(NSString*)GetCurrentVolumeName
{
	return m_CurrentVolumeName;
}

+(UserCollectionInfo*)GetCurrentCollection
{
	return m_CurrentCollection;
}

+(void)SetCurrentCollection:(UserCollectionInfo*)value
{
	m_CurrentCollection = value;
}

+(NSNumber*)GetCurrentVolumeId
{
	return m_CurrentVolumeId;
}

+(void)SetCurrentVolumeId:(NSNumber*)value
{
	m_CurrentVolumeId = value;
}

+(int)GetCurrentPage
{
	return m_CurrentPage;
}

+(void)SetCurrentPage:(int)value
{
	m_CurrentPage = value;
}

+(CollectionIssueInfo*)GetCurrentCollectionIssue
{
	return m_CurrentCollectionIssue;
}

+(void)SetCurrentCollectionIssue:(CollectionIssueInfo*)value
{
	m_CurrentCollectionIssue = value;
}

+(NSNumber*)GetCurrentCollectionId
{
	return m_CurrentCollectionId;
}

+(void)SetCurrentCollectionId:(NSNumber*)value
{
	m_CurrentCollectionId = value;
}

+(NSNumber*)GetCurrentUserId
{
	return m_UserId;
}


+(void)SetCurrentUserId:(NSNumber*)value
{
	m_UserId = value;
}

+(RKObjectManager*)CreateObjectManager
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKObjectMapping *collectionMapping = [self BuildCollectionResponseMapper];
		
	RKObjectMapping *collectionIssueMapping = [RKObjectMapping mappingForClass:[CollectionIssueInfo class]];
	[collectionIssueMapping addAttributeMappingsFromDictionary:@{
	 /// @"addedDateTime":@"addedDateTime",
	 @"coverImageLocation":@"coverImageLocation",
	 @"mobileImageLocation":@"mobileImageLocation",
	 @"issueTitle":@"issueTitle",
	 @"releaseYear":@"releaseYear",
	 @"issueNumber":@"issueNumber"
	 /*@"externalIssueID":@"externalIssueID",
	  @"issueDetailsLink":@"issueDetailsLink",
	  @"issueID":@"issueID",
	  ,
	  @"numberOfPages":@"numberOfPages",
	  @"releaseMonth":@"releaseMonth",
	  @"updatedDateTime":@"updatedDateTime",
	  @"volumeID":@"volumeID"*/
	 }];
	
	
	RKObjectMapping *collectionVolumeMapping = [RKObjectMapping mappingForClass:[VolumeInfo class]];
	[collectionVolumeMapping addAttributeMappingsFromDictionary:@{
	 @"volumeID":@"volumeID",
	 @"titleID":@"titleID",
	 @"volumeName":@"volumeName",
	 @"volumeNumber":@"volumeNumber",
	 @"numberOfIssues":@"numberOfIssues",
	 @"volumeStartYear":@"volumeStartYear",
	 @"volumeEndYear":@"volumeEndYear",
	 @"addedDateTime":@"addedDateTime"
	 }];
	
	 RKRelationshipMapping *issuesMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"issues"
																						toKeyPath:@"issues"
																						withMapping:collectionIssueMapping];
	
	RKRelationshipMapping *volumesMapping = [RKRelationshipMapping relationshipMappingFromKeyPath:@"volumes"
																					   toKeyPath:@"volumes"
																					 withMapping:collectionVolumeMapping];
	[collectionMapping addPropertyMapping:issuesMapping];
	[collectionMapping addPropertyMapping:volumesMapping];
	
	RKResponseDescriptor *collectionDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:collectionMapping pathPattern:nil keyPath:nil statusCodes:[NSIndexSet indexSetWithIndex:200]];
	
	[objectManager addResponseDescriptor:collectionDescriptor];

	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	return objectManager;
}

+(void)GetUserCollections:(NSNumber*)userId
						success:(void(^)(NSArray*))success
					    failure:(void(^)(NSError*))failure
{
	m_UserId = userId;
	
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections", userId];
	
	RKObjectManager *objectManager = [CollectionsService CreateObjectManager];
	
	 [objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
		{
			m_Collections = [mappingResult array];
			success(m_Collections);
		} failure:^(RKObjectRequestOperation *operation, NSError *error)
		{
			failure(error);
		}
	 ];
}

+(void)GetCollection:(NSNumber*)userId
			 collectionId:(NSNumber*)collectionId
			 page:(NSNumber*)page
			 success:(void(^)(UserCollectionInfo*))success
			 failure:(void(^)(NSError*))failure
{
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections/%@?page=%@", userId, collectionId, page];
	
	RKObjectManager *objectManager = [CollectionsService CreateObjectManager];
	
	[objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
	 {
		UserCollectionInfo *collection = [mappingResult firstObject];
		success(collection);
	 } failure:^(RKObjectRequestOperation *operation, NSError *error)
	 {
		failure(error);
	 }
	 ];
}

+(void)GetCollectionVolume:(NSNumber*)userId
						collectionId:(NSNumber*)collectionId
						volumeId:(NSNumber*)volumeId
						page:(NSNumber*)page
						success:(void(^)(UserCollectionInfo*))success
						failure:(void(^)(NSError*))failure
{
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections/%@/Volumes/%@?page=%@", userId, collectionId, volumeId, page];
	
	RKObjectManager *objectManager = [CollectionsService CreateObjectManager];
	
	[objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
		 {
		 UserCollectionInfo *collection = [mappingResult firstObject];
		 success(collection);
		 } failure:^(RKObjectRequestOperation *operation, NSError *error)
		 {
		 failure(error);
		 }
	 ];
}


+(void)AddIssueToCollection:(NSNumber*)userId
			   collectionId:(NSNumber*)collectionId
				   issueId:(NSNumber*)issueId
					success:(void(^)(NSString*))success
					failure:(void(^)(NSError*))failure
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	NSMutableDictionary	*requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setValue:userId forKey:@"collectorId"];
	[requestParameters setValue:collectionId forKey:@"userCollectionId"];
	[requestParameters setValue:issueId forKey:@"issueId"];
	
	[RKMIMETypeSerialization registerClass:[RKURLEncodedSerialization class] forMIMEType:@"text/plain"];
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections/%@/Issues/%@", userId, collectionId, issueId];
	
	NSLog(@"POST Path: %@", path);
	[objectManager postObject:path path:path parameters:requestParameters success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
		{
			success(@"Issue Added to Collection Successfully");
		}
	    failure:^(RKObjectRequestOperation *operation, NSError *error)
		{
			failure(error);
		}
	 ];
}

+(NSMutableDictionary*)BuildCreateCollectionCommand:(UserCollectionInfo*)collection
{
	NSMutableDictionary *command = [[NSMutableDictionary alloc] init];
	
	NSString *viewable = collection.isPubliclyViewable ? @"True" : @"False";
	
	[command setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Id"];
	[command setObject:collection.collectionName forKey:@"CollectionName"];
	[command setObject:collection.collectionType forKey:@"CollectionType"];
	[command setObject:collection.description forKey:@"Description"];
	[command setObject:viewable forKey:@"IsPubliclyViewable"];
	[command setObject:collection.userId forKey:@"UserId"];
	
	return command;
}

+(RKObjectMapping*)BuildCollectionRequestMapper
{
	RKObjectMapping *collectionMapping = [RKObjectMapping mappingForClass:[NSMutableDictionary class]];
	[collectionMapping addAttributeMappingsFromDictionary:@{
															@"Id":@"Id",
															@"CollectionName":@"CollectionName",
															@"CollectionType":@"CollectionType",
															@"Description":@"Description",
															@"IsPubliclyViewable":@"IsPubliclyViewable",
															@"UserId":@"UserId"
															}];
	
	return collectionMapping;
}

+(RKObjectMapping*)BuildCollectionResponseMapper
{
	RKObjectMapping *collectionMapping = [RKObjectMapping mappingForClass:[UserCollectionInfo class]];
	[collectionMapping addAttributeMappingsFromDictionary:@{
															@"collectionId":@"collectionId",
															@"collectionName":@"collectionName",
															@"description":@"description",
															@"userId":@"userId",
															@"collectionType":@"collectionType"
															}];
	
	return collectionMapping;
}

+(RKResponseDescriptor*)BuildCollectionResponseDescriptor
{
	RKObjectMapping *collectionMapping = [self BuildCollectionResponseMapper];
	
	NSRange statusCodeRange = NSMakeRange(200, 399);
	RKResponseDescriptor *collectionDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:collectionMapping																			pathPattern:nil
																		keyPath:nil
																		statusCodes:[NSIndexSet indexSetWithIndexesInRange: statusCodeRange]];
	
	return collectionDescriptor;
}

+(RKRequestDescriptor*)BuildCollectionRequestDescriptor:(NSNumber*)userId
{
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections/", userId];
	
	RKObjectMapping *collectionMapping = [self BuildCollectionRequestMapper];
	
	RKRequestDescriptor *collectionRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:collectionMapping
																							objectClass:[NSMutableDictionary class]
																							rootKeyPath:path];
	
	return collectionRequestDescriptor;
}

+(RKObjectManager*)BuildCollectionCreationManager:(NSNumber*)userId
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *manager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKRequestDescriptor *requestDescriptor = [self BuildCollectionRequestDescriptor:userId];
	[manager addRequestDescriptor:requestDescriptor];
	
	RKResponseDescriptor *responseDescriptor = [self BuildCollectionResponseDescriptor];
	[manager addResponseDescriptor:responseDescriptor];
	
	RKResponseDescriptor *errorDescriptor = [UtilityServices BuildErrorResponseDescriptor];
	[manager addResponseDescriptor:errorDescriptor];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[manager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	return manager;
}

+(void)CreateNewCollection:(UserCollectionInfo*)collection
				   success:(void(^)(UserCollectionInfo*))success
				   failure:(void(^)(NSError*))failure
{
	//call API to create new collection
	RKObjectManager *objectManager = [self BuildCollectionCreationManager:collection.userId];
	
	NSString *path = [NSString stringWithFormat:@"/Collectors/%@/Collections/", collection.userId];

	NSMutableDictionary *command = [self BuildCreateCollectionCommand:collection];
	
	[objectManager postObject:path
						 path:path
				   parameters:command
					  success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
						{
							UserCollectionInfo *collection = [mappingResult firstObject];
							success(collection);
						}
					  failure:^(RKObjectRequestOperation *operation, NSError *error)
						{
							failure(error);
						}
	 ];
}

@end
