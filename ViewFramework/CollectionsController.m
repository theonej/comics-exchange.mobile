//
//  CollectionsController.m
//  ViewFramework
//
//  Created by J Henry on 7/29/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "RestKit/RestKit.h"

#import "CollectionsController.h"
#import "CollectionsService.h"
#import "UserCollectionInfo.h"

@interface CollectionsController ()

@end

@implementation CollectionsController

NSArray *m_Collections;

-(void)viewDidLoad
{
	[[self spinner_GetUserCollections] startAnimating];
	
	NSNumber *userId = [CollectionsService GetCurrentUserId];
	[CollectionsService GetUserCollections:userId
								    success:^(NSArray *collections)
									{
									[[self spinner_GetUserCollections] stopAnimating];
										m_Collections = collections;
										[self.tableView reloadData];
									}
									failure:^(NSError *error)
									{
									
									[[self spinner_GetUserCollections] stopAnimating];
									
										UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																						message:[error description]
																						delegate:nil
																						cancelButtonTitle:@"OK"
																						otherButtonTitles:nil];
										
										[alert show];
									}
	 ];
	
	[super viewDidLoad];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [m_Collections count];
}

-(UITableViewCell *)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *collectionCell = @"CollectionCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
	}
	
	UserCollectionInfo *selectedCollection =  [m_Collections objectAtIndex:indexPath.row];
	
	cell.textLabel.text = selectedCollection.collectionName;
	cell.textColor = [UIColor whiteColor];
	
	//cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"disclosure.png"]];
	return cell;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UserCollectionInfo *selectedCollection =  [m_Collections objectAtIndex:indexPath.row];
	
	NSNumber *collectionId  =  selectedCollection.collectionId;
	[CollectionsService SetCurrentCollectionId:collectionId];
	[CollectionsService SetCurrentPage:0];
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Collections" style:UIBarButtonItemStylePlain target:nil action:nil];
	
	[self performSegueWithIdentifier:@"segue_ShowCollectionVolumes" sender:self];
}

@end
