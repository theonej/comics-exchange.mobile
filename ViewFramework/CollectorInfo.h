//
//  CollectorInfo.h
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectorInfo : NSObject <NSKeyedArchiverDelegate>

@property (nonatomic, strong) NSString *authenticationTicket;
@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *emailAddress;
@property (nonatomic, strong) NSDate *createdDateTime;
@property (nonatomic, strong) NSString *userIconURL;
@property (nonatomic, strong) NSString *password;

@end
