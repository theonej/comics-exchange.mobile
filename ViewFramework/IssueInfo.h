//
//  IssueInfo.h
//  ViewFramework
//
//  Created by J Henry on 8/12/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IssueInfo : NSObject

@property (nonatomic, strong) NSString *mobileImageLocation;
@property (nonatomic, strong) NSNumber *issueID;
@property (nonatomic, strong) NSString *issueNumber;
@property (nonatomic, strong) NSNumber *volumeID;
@property (nonatomic, strong) NSString *issueTitle;
@property (nonatomic, strong) NSString *numberOfPages;
@property (nonatomic, strong) NSString *releaseMonth;
@property (nonatomic, strong) NSString *releaseYear;
@property (nonatomic, strong) NSString *isDeleted;
@property (nonatomic, strong) NSString *addedDateTime;
@property (nonatomic, strong) NSString *updatedDateTime;
@property (nonatomic, strong) NSString *issueDetailsLink;

@end
