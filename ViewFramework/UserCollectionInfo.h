//
//  UserCollectionInfo.h
//  ViewFramework
//
//  Created by J Henry on 7/31/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserCollectionInfo : NSObject

@property (nonatomic) NSNumber *collectionId;
@property (strong, nonatomic) NSString *collectionName;
@property (strong, nonatomic) NSString *description;
@property (nonatomic, copy) NSNumber *userId;
@property  (nonatomic, copy) NSNumber *collectionType;
@property (nonatomic) NSArray *issues;
@property (nonatomic) NSArray *volumes;
@property (nonatomic) BOOL isPubliclyViewable;
@property (nonatomic) NSArray *all;
@end
