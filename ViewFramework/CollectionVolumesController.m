//
//  CollectionVolumesController.m
//  ViewFramework
//
//  Created by J Henry on 1/6/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "CollectionVolumesController.h"
#import "CollectionsService.h"
#import "UserCollectionInfo.h"
#import "CollectionIssueInfo.h"

@interface CollectionVolumesController ()

@end

@implementation CollectionVolumesController

NSNumber *m_CollectionId;
NSNumber *m_UserId;
NSNumber *m_VolumeId;

static int m_Page;

UserCollectionInfo *m_Collection;

-(IBAction)handleSwipeRight:(UISwipeGestureRecognizer *)recognizer
{
	m_Page = m_Page + 1;
	
	[CollectionsService SetCurrentPage:m_Page];
	
	[self GetCollection];
	
}

-(IBAction)handleSwipeLeft:(UISwipeGestureRecognizer *)recognizer
{
	m_Page = m_Page - 1;
	if(m_Page < 0)
		m_Page = 0;
	
	[CollectionsService SetCurrentPage:m_Page];
	
	[self GetCollection];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	[[self spinner_GetVolumeIssues] startAnimating];
	
	m_UserId = [CollectionsService GetCurrentUserId];
	m_CollectionId = [CollectionsService GetCurrentCollectionId];
	m_VolumeId = [CollectionsService GetCurrentVolumeId];
	
	m_Page = [CollectionsService GetCurrentPage];
	
	self.title = [CollectionsService GetCurrentVolumeName];
	
	[self GetCollection];
	
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)GetCollection
{
	[CollectionsService GetCollectionVolume:m_UserId
							   collectionId:m_CollectionId
								   volumeId:m_VolumeId
									   page:[NSNumber numberWithInt:m_Page]
									success:^(UserCollectionInfo *collection)
								    {
										[[self spinner_GetVolumeIssues] stopAnimating];
									
										m_Collection = collection;
										
										[self.tableView reloadData];
										
									} failure:^(NSError *error) {
										
										[[self spinner_GetVolumeIssues] stopAnimating];
										
										UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																						message:[error description]
																					   delegate:nil
																			  cancelButtonTitle:@"OK"
																			  otherButtonTitles:nil];
										
										[alert show];
									}];
	
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [m_Collection.issues count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *collectionCell = @"Issue Cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
		{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
		}
	
    CollectionIssueInfo *selectedIssue =  [m_Collection.issues objectAtIndex:indexPath.row];
	cell.textLabel.text = [NSString stringWithFormat:@"#%@ - %@",
						   selectedIssue.issueNumber,
						   selectedIssue.issueTitle];
	
	cell.textColor = [UIColor whiteColor];
	
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionIssueInfo	*issue = m_Collection.issues[indexPath.row];
	[CollectionsService SetCurrentCollectionIssue:issue];
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Issues" style:UIBarButtonItemStylePlain target:nil action:nil];
	
	[self performSegueWithIdentifier:@"segue_ShowCoverImage" sender:issue];
}

@end
