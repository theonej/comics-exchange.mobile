//
//  CollectorsService.h
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectorInfo.h"

@interface CollectorsService : NSObject

+(void)RegisterCollector:(CollectorInfo*)collector
				 success:(void(^)(CollectorInfo*))success
				 failure:(void(^)(NSError*))failure;

+(void)AuthenticateCollector:(CollectorInfo*)collector
					 success:(void(^)(CollectorInfo*))success
					 failure:(void(^)(NSError*))failure;

+(void)ResetPassword:(NSString*)emailAddress
			 success:(void(^)(NSString*))success
			 failure:(void(^)(NSError*))failure;
@end
