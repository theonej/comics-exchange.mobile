	//
//  LoginController.m
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "LoginController.h"
#import "CollectorsService.h"
#import "ErrorInfo.h"
#import "StorageServices.h"

@interface LoginController ()<UITextFieldDelegate>
@end

@implementation LoginController

NSString *m_EmailAddress;
NSString *m_Password;


- (IBAction)input_EmailAddress:(UITextField *)sender
{
	m_EmailAddress = sender.text;
	sender.delegate = self;
}

- (IBAction)input_Password:(UITextField *)sender
{
	m_Password = sender.text;
	sender.delegate = self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self dismissViewControllerAnimated:TRUE completion:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	CollectorInfo *collector = [StorageServices GetCollectorInfo];
	if(collector)
		{
			[self performSegueWithIdentifier:@"seque_goToHomeScreen" sender:self];
		}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)button_Register_Click:(id)sender
{
	CollectorInfo *collector = [CollectorInfo new];
	collector.emailAddress = m_EmailAddress;
	collector.password = m_Password;
	
	//register the collector with the email address and password supplied
	[CollectorsService RegisterCollector:collector
								 success:^(CollectorInfo *collector)
								{
									NSString* successMessage = @"Registration Successful!";
								
									UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
																				message:successMessage
																			   delegate:nil
																	  cancelButtonTitle:@"OK"
																	  otherButtonTitles:nil];
								
									[alert show];
									
									//save the authenticated user info to the device so we can log them in
									[StorageServices SaveCollectorInfo:collector];
								
									[self performSegueWithIdentifier:@"seque_goToHomeScreen" sender:self];
								}
								 failure:^(NSError *error)
								{
									NSArray *errorMessages = [[error userInfo] objectForKey:@"RKObjectMapperErrorObjectsKey"];
												  
									ErrorInfo *errorMessage = [errorMessages objectAtIndex:0];
								
									UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													 message:errorMessage.message
													delegate:nil
										            cancelButtonTitle:@"OK"
										            otherButtonTitles:nil];
	 
									[alert show];
								}
	 ];
}

- (IBAction)button_Login_Click:(id)sender
{
	CollectorInfo *collector = [CollectorInfo new];
	collector.emailAddress = m_EmailAddress;
	collector.password = m_Password;
	
	//register the collector with the email address and password supplied
	[CollectorsService AuthenticateCollector:collector
								 success:^(CollectorInfo *collector)
									{
										//save the authenticated user info to the device so we can log them in
										[StorageServices SaveCollectorInfo:collector];
	 
										[self performSegueWithIdentifier:@"seque_goToHomeScreen" sender:self];
									}
								 failure:^(NSError *error)
									{
										NSArray *errorMessages = [[error userInfo] objectForKey:@"RKObjectMapperErrorObjectsKey"];
	 
										ErrorInfo *errorMessage = [errorMessages objectAtIndex:0];
	 
										UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																						message:errorMessage.message
																					    delegate:nil
																						cancelButtonTitle:@"OK"
																						otherButtonTitles:nil];
	 
										[alert show];
									}
	 ];
}
- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
	
	[theTextField resignFirstResponder];
	
    return YES;
}
- (IBAction)button_ForgotPasswordClick:(UIButton *)sender
{
	[StorageServices ClearCollectorInfo];
	
	[self performSegueWithIdentifier:@"segue_GoToResetPassword" sender:self];
}
@end
