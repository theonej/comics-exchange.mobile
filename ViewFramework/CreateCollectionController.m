//
//  CreateCollectionController.m
//  ViewFramework
//
//  Created by J Henry on 1/6/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "CreateCollectionController.h"
#import "StorageServices.h"
#import "CollectionsService.h"
#import "UserCollectionInfo.h"
#import "CollectorInfo.h"
#import "ErrorInfo.h"

@interface CreateCollectionController ()<UITextFieldDelegate>

@end

@implementation CreateCollectionController

NSString *m_CollectionName;

- (IBAction)text_CollectionName:(UITextField *)sender
{
	m_CollectionName = sender.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField
{
	[theTextField resignFirstResponder];
	
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)button_CreateCollection_Click:(id)sender
{
	CollectorInfo *collector = [StorageServices GetCollectorInfo];
	
	UserCollectionInfo *collection = [UserCollectionInfo alloc];
	collection.userId = collector.userId;
	collection.collectionName = m_CollectionName;
	collection.description = @"";
	collection.collectionType = [NSNumber numberWithInt:0];
	collection.isPubliclyViewable = TRUE;
	
	[CollectionsService CreateNewCollection:collection
									success:^(UserCollectionInfo *collection)
									{
										NSString* successMessage = @"Collection Created!";
									
										UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
																					message:successMessage
																				   delegate:nil
																		  cancelButtonTitle:@"OK"
																		  otherButtonTitles:nil];
									
										[alert show];
									
										[self performSegueWithIdentifier:@"seque_goToMyCollections" sender:self];
									}
									failure:^(NSError *error)
									{
										NSArray *errorMessages = [[error userInfo] objectForKey:@"RKObjectMapperErrorObjectsKey"];
									
										ErrorInfo *errorMessage = [errorMessages objectAtIndex:0];
									
										UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																					message:errorMessage.message
																				   delegate:nil
																		  cancelButtonTitle:@"OK"
																		  otherButtonTitles:nil];
									
										[alert show];
									}
	 ];
}

@end
