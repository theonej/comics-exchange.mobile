//
//  IssueImageCell.m
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "IssueImageCell.h"

@implementation IssueImageCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)setCoverImageUrl:(NSString *)coverImageUrl
{
	coverImageUrl = @"https://s3.amazonaws.com/mobile.comics-exchange.com/noimage.jpg";
	NSURL *imageUrl = [NSURL URLWithString:coverImageUrl];
	NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
	
	UIImage *image = [UIImage imageWithData:imageData];
	self.coverImageView.image = image;
}

-(void)setIssueTitle:(NSString *)issueTitle
{
	self.label_IssueTitle.text = issueTitle;
}

-(void)setYear:(NSString *)year
{
	self.label_ReleaseYear.text = year;
}
-(void)setIssue:(CollectionIssueInfo *)issue
{
	[self setCoverImageUrl:issue.mobileImageLocation];
	
	NSString *issueTitle = [NSString stringWithFormat:@"%@ - #%@", issue.issueNumber, issue.issueTitle];
	[self setIssueTitle:issueTitle];
	[self setYear:issue.releaseYear];
}
@end
