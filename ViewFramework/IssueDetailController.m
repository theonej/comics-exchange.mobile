//
//  IssueDetailController.m
//  ViewFramework
//
//  Created by J Henry on 8/13/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "IssueDetailController.h"
#import "issueInfo.h"
#import "SearchService.h"

@interface IssueDetailController ()

@property (nonatomic, strong) IBOutlet UIImageView *coverImageView;

@end

@implementation IssueDetailController

IssueInfo *m_Issue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	m_Issue = [SearchService GetCurrentIssue];
	
	[[self spinner_LoadIssueDetails] startAnimating];
	[self startTimer];
	
    [super viewDidLoad];
}

- (void) startTimer
{
	[NSTimer scheduledTimerWithTimeInterval:0.1
									 target:self
								   selector:@selector(tick:)
								   userInfo:nil
									repeats:NO];
}

- (void) tick:(NSTimer *) timer
{
	[self ShowCoverImage:m_Issue.mobileImageLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)ShowCoverImage:(NSString*)coverImageUrl
{
	NSLog (@"Image URL: %@", coverImageUrl);
	NSURL *imageUrl = [NSURL URLWithString:coverImageUrl];
	NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
	
	UIImage *image = [UIImage imageWithData:imageData];
	self.coverImageView.image = image;
	
	[[self spinner_LoadIssueDetails]stopAnimating];
}

@end
