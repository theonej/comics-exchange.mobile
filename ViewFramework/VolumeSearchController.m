//
//  VolumeSearchController.m
//  ViewFramework
//
//  Created by J Henry on 8/12/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "VolumeSearchController.h"
#import "SearchService.h"

@interface VolumeSearchController ()

@end

@implementation VolumeSearchController

NSString *m_VolumeName;
NSArray *m_Volumes;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
	[[self spinner_SearchVolumes] startAnimating];
	
	m_Volumes = [NSMutableArray array];
	[self.tableView reloadData];
	
	[self SearchVolumes];
	
    [super viewDidLoad];
}

-(void)SearchVolumes
{
	m_VolumeName = [SearchService GetVolumeSearchTerm];
	[SearchService SearchVolumes:m_VolumeName page:0
						 success:^(NSArray *volumes)
						 {
							[[self spinner_SearchVolumes] stopAnimating];
							
							m_Volumes = volumes;
							[self.tableView reloadData];
						 }
						 failure:^(NSError *error)
						 {
						 
						 [[self spinner_SearchVolumes] stopAnimating];
						 
							 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
																			 message:[error description]
																			delegate:nil
																   cancelButtonTitle:@"OK"
																   otherButtonTitles:nil];
							 
							 [alert show];
						 }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [m_Volumes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *collectionCell = @"CollectionCell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:collectionCell];
	
	if(cell == nil)
		{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:collectionCell];
		}
	
    VolumeInfo *selectedVolume =  [m_Volumes objectAtIndex:indexPath.row];
	cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)",
						   selectedVolume.volumeName,
						   selectedVolume.volumeStartYear];
    
	cell.textColor = [UIColor whiteColor];
	
    return cell;
}

#pragma mark - Table view delegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    VolumeInfo *selectedVolume =  [m_Volumes objectAtIndex:indexPath.row];
	
	[SearchService SetCurrentVolume:selectedVolume];
	[SearchService SetCurrentPage:0];
	
	[self performSegueWithIdentifier:@"segue_ShowSearchVolumeIssues" sender:self];
}


@end
