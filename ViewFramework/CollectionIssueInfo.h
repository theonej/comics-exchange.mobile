//
//  CollectionIssueInfo.h
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CollectionIssueInfo : NSObject

//@property (nonatomic, strong) NSString *addedDateTime;
@property (nonatomic, strong) NSString *coverImageLocation;
@property (nonatomic, strong) NSString *mobileImageLocation;
@property (nonatomic, strong) NSString *issueTitle;
@property (nonatomic, strong) NSString *issueNumber;
@property (nonatomic, strong) NSString *releaseYear;
/*@property (nonatomic, strong) NSNumber *externalIssueID;
@property (nonatomic, strong) NSString *issueDetailsLink;
@property (nonatomic, strong) NSNumber *issueID;

@property (nonatomic, strong) NSNumber *numberOfPages;
@property (nonatomic, strong) NSString *releaseMonth;
@property (nonatomic, strong) NSString *updatedDateTime;
@property (nonatomic, strong) NSNumber *volumeID;*/

@end
