//
//  IssueImageCell.h
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionIssueInfo.h"

@interface IssueImageCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *label_ReleaseYear;
@property (nonatomic, strong) IBOutlet UIImageView *coverImageView;
@property (nonatomic, strong) IBOutlet UILabel *label_IssueTitle;
@property (nonatomic, strong) CollectionIssueInfo *issue;

@end
