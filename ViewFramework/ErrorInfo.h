//
//  ErrorInfo.h
//  ViewFramework
//
//  Created by J Henry on 1/5/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorInfo : NSObject

@property (nonatomic, strong) NSString *message;

@end
