//
//  VolumeSearchController.h
//  ViewFramework
//
//  Created by J Henry on 8/12/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VolumeSearchController : UITableViewController

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner_SearchVolumes;
@end
