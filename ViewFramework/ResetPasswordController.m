//
//  ResetPasswordController.m
//  ViewFramework
//
//  Created by J Henry on 2/10/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "ResetPasswordController.h"
#import "CollectorsService.h"
#import "ErrorInfo.h"

@implementation ResetPasswordController

NSString* m_EmailAddress;

- (IBAction)input_EmailAddress:(UITextField*)sender
{
	m_EmailAddress = sender.text;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)button_ResetPasswordClicked:(id)sender
{
	[self ResetPassword];
}

-(void)ResetPassword
{
	[CollectorsService ResetPassword:m_EmailAddress
								success:^(NSString* successMessage)
								{
									UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success!"
													message:successMessage
													delegate:nil
													cancelButtonTitle:@"OK"
													otherButtonTitles:nil];
	 
									[alert show];
	 
									[self performSegueWithIdentifier:@"segue_GoBackToLogin" sender:self];
								}
							 failure:^(NSError *error)
							{
								NSArray *errorMessages = [[error userInfo] objectForKey:@"RKObjectMapperErrorObjectsKey"];
	 
							    ErrorInfo *errorMessage = [errorMessages objectAtIndex:0];
	 
							    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
													message:errorMessage.message
													delegate:nil
													cancelButtonTitle:@"OK"
													otherButtonTitles:nil];
	 
								[alert show];
							}
	 ];
}

@end
