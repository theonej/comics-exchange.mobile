//
//  CollectorsService.m
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import "RestKit/RestKit.h"
#import "CollectorsService.h"
#import "ErrorInfo.h"
#import "UtilityServices.h"

@implementation CollectorsService



+(RKObjectMapping*)BuildCollectorObjectMapping
{
	RKObjectMapping *collectorMapping = [RKObjectMapping mappingForClass:[CollectorInfo class]];
	[collectorMapping addAttributeMappingsFromDictionary:@{
														   @"authenticationTicket":@"authenticationTicket",
														   @"userID":@"userId",
														   @"userName":@"userName",
														   @"emailAddress":@"emailAddress",
														   @"createdDateTime":@"createdDateTime",
														   @"userIconURL":@"userIconURL"
														   }];
	
	return collectorMapping;
}

+(RKResponseDescriptor*)BuildCollectorResponseDescriptor
{
	RKObjectMapping *collectorMapping = [self BuildCollectorObjectMapping];
	
	NSRange statusCodeRange = NSMakeRange(200, 399);
	RKResponseDescriptor *collectorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:collectorMapping															pathPattern:nil
														keyPath:nil
														statusCodes:[NSIndexSet indexSetWithIndexesInRange:statusCodeRange]];
	return collectorDescriptor;
}

+(RKRequestDescriptor*)BuildCollectorRequestDescriptor
{
	NSString *path = @"/Collectors/";
	
	RKObjectMapping *collectorMapping = [RKObjectMapping mappingForClass:[NSMutableDictionary class]];
	[collectorMapping addAttributeMappingsFromDictionary:@{
														   @"Id":@"Id",
														   @"EmailAddress":@"EmailAddress",
														   @"Password":@"Password"
														   }];
	
	RKRequestDescriptor *collectorRequestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:collectorMapping
																							objectClass:[NSMutableDictionary class]
																							rootKeyPath:path];
	
	return collectorRequestDescriptor;
}

+(RKObjectManager*)CreateCollectorObjectManager:(CollectorInfo*)collector
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKResponseDescriptor *collectorDescriptor = [self BuildCollectorResponseDescriptor];
	[objectManager addResponseDescriptor:collectorDescriptor];
	
	RKResponseDescriptor *errorResponseDescriptor = [UtilityServices BuildErrorResponseDescriptor];
	[objectManager addResponseDescriptor:errorResponseDescriptor];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	return objectManager;
}

+(NSMutableDictionary*)BuildCreateCollectorCommand:(CollectorInfo*)collector
{
	NSMutableDictionary *command = [[NSMutableDictionary alloc] init];
	
	[command setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Id"];
	[command setObject:collector.emailAddress forKey:@"EmailAddress"];
	[command setObject:collector.password forKey:@"Password"];
	
	return command;
}

+(NSMutableDictionary*)BuildResetPasswordCommand:(NSString*)emailAddress
{
	NSMutableDictionary *command = [[NSMutableDictionary alloc] init];
	
	[command setObject:@"00000000-0000-0000-0000-000000000000" forKey:@"Id"];
	[command setObject:emailAddress forKey:@"emailAddress"];
	
	return command;
}

+(RKObjectManager*)CreateRegistrationCommandObjectManager:(CollectorInfo*)collector
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];

	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKRequestDescriptor *collectorRequestDescriptor = [self BuildCollectorRequestDescriptor];
	[objectManager addRequestDescriptor:collectorRequestDescriptor];
	
	RKResponseDescriptor *collectorResponseDescriptor = [self BuildCollectorResponseDescriptor];
	[objectManager addResponseDescriptor:collectorResponseDescriptor];
	
	RKResponseDescriptor *errorResponseDescriptor = [UtilityServices BuildErrorResponseDescriptor];
	[objectManager addResponseDescriptor:errorResponseDescriptor];
	
	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	return objectManager;
}

+(RKObjectManager*)CreateResetPasswordObjectManager
{
	NSURL *baseUrl = [UtilityServices GetBaseUrl];
	RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseUrl];
	
	RKResponseDescriptor *collectorResponseDescriptor = [self BuildCollectorResponseDescriptor];
	[objectManager addResponseDescriptor:collectorResponseDescriptor];
	
	RKResponseDescriptor *errorResponseDescriptor = [UtilityServices BuildErrorResponseDescriptor];
	[objectManager addResponseDescriptor:errorResponseDescriptor];

	NSString *authenticationToken = [UtilityServices GetUserAuthenticationToken];
	[objectManager.HTTPClient setDefaultHeader:@"Comics-Exchange.UserToken" value:authenticationToken];
	
	return objectManager;
}

+(void)RegisterCollector:(CollectorInfo*)collector
				 success:(void(^)(CollectorInfo*))success
				   failure:(void(^)(NSError*))failure
{
	NSString *path = @"/Collectors/";
	
	RKObjectManager *objectManager = [self CreateRegistrationCommandObjectManager:collector];

	NSMutableDictionary *command = [self BuildCreateCollectorCommand:collector];
	
	[RKMIMETypeSerialization registerClass:[RKURLEncodedSerialization class] forMIMEType:@"text/plain"];
	
	[objectManager postObject:path
				   path:path
				   parameters:command
					  success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
					  {
						[self AuthenticateCollector:collector success:success failure:failure];
					  }
					  failure:^(RKObjectRequestOperation *operation, NSError *error)
					  {
						failure(error);
					  }
	 ];
}

+(void)AuthenticateCollector:(CollectorInfo*)collector
				 success:(void(^)(CollectorInfo*))success
				 failure:(void(^)(NSError*))failure
{
	NSString *path = @"Collectors/";
	
	RKObjectManager *objectManager = [CollectorsService CreateCollectorObjectManager:collector];
	
	[[objectManager HTTPClient] setAuthorizationHeaderWithUsername:collector.emailAddress password:collector.password];
	
	//send GET request
	[objectManager getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
	 {
		CollectorInfo *authenticatedCollector = [mappingResult firstObject];
		success(authenticatedCollector);
	 } failure:^(RKObjectRequestOperation *operation, NSError *error)
	 {
		failure(error);
	 }
	 ];
}

+(void)ResetPassword:(NSString*)emailAddress
			 success:(void(^)(NSString*))success
			 failure:(void(^)(NSError*))failure
{
	RKObjectManager *objectManager = [self CreateResetPasswordObjectManager];
	
	NSMutableDictionary	*command = [self BuildResetPasswordCommand:emailAddress];
	
	[RKMIMETypeSerialization registerClass:[RKURLEncodedSerialization class] forMIMEType:@"text/plain"];
	NSString *path = @"/PasswordReset/";
	
	NSLog(@"POST Path: %@", path);
	[objectManager postObject:path
					path:path
					parameters:command
					success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult)
					{
						success(@"Password reset.  Please check your email for your new password.");
					}
					failure:^(RKObjectRequestOperation *operation, NSError *error)
					{
						failure(error);
					}
	 ];
}

@end
