//
//  UtilityServices.h
//  Comics-Exchange.Mobile
//
//  Created by J Henry on 4/28/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RestKit/RestKit.h"

@interface UtilityServices : NSObject

	+(NSURL*)GetBaseUrl;
	+(NSString*)ConvertToBase64String:(NSString*)inputString;
	+(RKResponseDescriptor*)BuildErrorResponseDescriptor;
	+(NSString*)GetUserAuthenticationToken;
@end
