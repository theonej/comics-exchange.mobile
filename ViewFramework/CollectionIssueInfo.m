//
//  CollectionIssueInfo.m
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "CollectionIssueInfo.h"

@implementation CollectionIssueInfo

//@synthesize addedDateTime;
@synthesize coverImageLocation;
@synthesize mobileImageLocation;
@synthesize issueTitle;
@synthesize issueNumber;
@synthesize releaseYear;
/*@synthesize externalIssueID;
@synthesize issueDetailsLink;
@synthesize issueID;

@synthesize numberOfPages;
@synthesize releaseMonth;

@synthesize updatedDateTime;
@synthesize volumeID;*/

@end
