//
//  UserCollectionInfo.m
//  ViewFramework
//
//  Created by J Henry on 7/31/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "UserCollectionInfo.h"

@implementation UserCollectionInfo

@synthesize collectionId;
@synthesize collectionName;
@synthesize description;
@synthesize userId;
@synthesize collectionType;
@synthesize issues;
@synthesize volumes;
@synthesize isPubliclyViewable;
@end
