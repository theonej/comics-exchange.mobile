//
//  ImageDetailsController.m
//  ViewFramework
//
//  Created by J Henry on 8/5/13.
//  Copyright (c) 2013 J Henry. All rights reserved.
//

#import "ImageDetailsController.h"
#import "CollectionsService.h"
#import "CollectionIssueInfo.h"


@implementation ImageDetailsController

CollectionIssueInfo *m_CurrentIssue;

-(IBAction)handleSwipe:(UISwipeGestureRecognizer *)recognizer
{
	
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	
	m_CurrentIssue = [CollectionsService GetCurrentCollectionIssue];
	
	self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Issues" style:UIBarButtonItemStylePlain target:nil action:nil];
	
	[[self spinner_ImageDownload] startAnimating];
	[self startTimer];
}
- (void) startTimer
{
	[NSTimer scheduledTimerWithTimeInterval:0.1
									 target:self
								   selector:@selector(tick:)
								   userInfo:nil
									repeats:NO];
}

- (void) tick:(NSTimer *) timer
{
	[self ShowCoverImage:m_CurrentIssue.mobileImageLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ShowCoverImage:(NSString*)coverImageUrl
{
	NSLog (@"Image URL: %@", coverImageUrl);
	NSURL *imageUrl = [NSURL URLWithString:coverImageUrl];
	NSData *imageData = [NSData dataWithContentsOfURL:imageUrl];
	
	UIImage *image = [UIImage imageWithData:imageData];
	self.coverImageView.image = image;
	
	[[self spinner_ImageDownload] stopAnimating];
}

@end
