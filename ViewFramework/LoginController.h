//
//  LoginController.h
//  ViewFramework
//
//  Created by J Henry on 1/2/14.
//  Copyright (c) 2014 J Henry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController <UITextFieldDelegate>

@end
